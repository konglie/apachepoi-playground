package kkurawal.demopoi.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konglie on 10/31/2016.
 * http://www.kurungkurawal.com
 *
 * Library required: Apache POI, https://poi.apache.org/
 */
public class GetCell {
	static final String excelFile = "demo-files/get-cell.xlsx";

	/**
	 * THIS IS created to ONLY FIT the DEMO, not include the world-war solution.
	 * THIS IS THE MAIN DEMO of getting cell content from XLSX file with Apache POI
	 * Expected output

	 ISI Dari CELL atas (hijau)
		 C2 = ATAS 1
		 D2 = ATAS 2
		 E2 = ATAS 3
		 F2 = ATAS 4
		 G2 = ATAS 5

	 ISI Dari CELL kiri (merah)
		 B3 = KIRI 1
		 B4 = KIRI 2
		 B5 = KIRI 3
		 B6 = KIRI 4
		 B7 = KIRI 5

	 ISI Dari CELL tengah
		 C3 = AK 11
		 C4 = AK 21
		 C5 = AK 31
		 C6 = AK 41
		 C7 = AK 51
		 D3 = AK 12
		 D4 = AK 22
		 D5 = AK 32
		 D6 = AK 42
		 D7 = AK 52
		 E3 = AK 13
		 E4 = AK 23
		 E5 = AK 33
		 E6 = AK 43
		 E7 = AK 53
		 F3 = AK 14
		 F4 = AK 24
		 F5 = AK 34
		 F6 = AK 44
		 F7 = AK 54
		 G3 = AK 15
		 G4 = AK 25
		 G5 = AK 35
		 G6 = AK 45
		 G7 = AK 55
	 *
	 * @param addresses
	 * @param sheet
	 */
	public static void printCellContent(List<String> addresses, Sheet sheet){
		CellReference cellReference;
		Row row;
		Cell cell;
		for(String address : addresses){
			// langsung akses ke cell yang diinginkan
			cellReference = new CellReference(address);
			row = sheet.getRow(cellReference.getRow());
			cell = row.getCell(cellReference.getCol());

			stdout(String.format("\t%s = %s", address, cell.getStringCellValue()));
		}
		stdout("");
	}

	public static void main(String args[]){
		try {
			FileInputStream fis = new FileInputStream(excelFile);
			Workbook book = new XSSFWorkbook(fis);

			// data ada di Sheet 1
			Sheet sheet = book.getSheetAt(0);

			// data berwarna hijau ada dari C2 s/d G2
			List<String> greenCells = getCellRange("c2", "g2");
			stdout("ISI Dari CELL atas (hijau)");
			printCellContent(greenCells, sheet);

			// data merah, dari B3 s/d B7
			List<String> reds = getCellRange("b3", "b7");
			stdout("ISI Dari CELL kiri (merah)");
			printCellContent(reds, sheet);

			// putih atau isinya, C3 s/d G7
			List<String> whites = getCellRange("c3", "g7");
			stdout("ISI Dari CELL tengah");
			printCellContent(whites, sheet);

		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public static void stdout(Object o){
		System.out.println(o);
	}

	/**
	 * THIS IS created to ONLY FIT the DEMO, not include the world-war solution.
	 *
	 * @param from Cell Address, eg. A1
	 * @param to Cell address, eg. Z99
	 * @return the list of cells between from and to
	 */
	public static List<String> getCellRange(String from, String to){
		List<String> cells = new ArrayList<String>();
		from = from.toUpperCase();
		to = to.toUpperCase();

		char sfrom = from.charAt(0);
		char sto = to.charAt(0);
		int nfrom = Integer.parseInt(from.substring(1, from.length()));
		int nto = Integer.parseInt(to.substring(1, to.length()));
		for(; sfrom <= sto; sfrom++){
			for(int i = nfrom; i <= nto; i++){
				cells.add(String.format("%s%s", sfrom, i));
			}
		}

		return cells;
	}
}
